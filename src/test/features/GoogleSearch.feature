Feature: Use Google to search for stuff

  Scenario: As a Google user, I want to search for Bob Lazar
    When I navigate to the Google home page
    And I input Bob Lazar into the search field
    And I click the Google Search button
    Then I verify the search results page is displayed

  Scenario: As a Google user, I want to search for How to cook a steak
    When I navigate to the Google home page
    And I input How to cook a steak into the search field
    And I click the Google Search button
    Then I verify the search results page is displayed