package helpers;

import org.openqa.selenium.WebElement;
import pages.WebPage;

public class CustomWebElement {

    private WebElement webElement;

    public CustomWebElement(WebElement webElement) {
        this.webElement = webElement;
    }

    public void click() {
        WebPage.waitForVisibilityOf(webElement);
        webElement.click();
    }

}
