package pages;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Output;

public class GoogleResultsPage extends WebPage {

    public GoogleResultsPage() {
        super("#resultStats");
    }

    @Getter
    @FindBy(css = "[title='Search']")
    private WebElement searchInput;

    @Getter
    @FindBy(css = "[aria-label='Google Search']")
    private WebElement searchButton;

    @Getter
    @FindBy(linkText = "All")
    private WebElement allTab;

    @Getter
    @FindBy(linkText = "Maps")
    private WebElement mapsTab;

    @Getter
    @FindBy(linkText = "Shopping")
    private WebElement shoppingTab;

    @Getter
    @FindBy(linkText = "Videos")
    private WebElement videosTab;

    @Getter
    @FindBy(linkText = "Images")
    private WebElement imagesTab;

    @Getter
    @FindBy(linkText = "Books")
    private WebElement booksTab;

    @Getter
    @FindBy(linkText = "News")
    private WebElement newsTab;

}
