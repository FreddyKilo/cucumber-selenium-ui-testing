package pages;

import helpers.CustomWebElement;
import helpers.URL;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import step_definitions.Hooks;
import utils.Output;

public class GoogleSearchPage extends WebPage {

    public GoogleSearchPage() {
        super("[value='Google Search']");
    }

    public static void navigateTo() {
        Output.log("Navigating to Google home page...");
        Hooks.webDriver.get(URL.Google);
    }

    @Getter
    @FindBy(css = "[title='Search']")
    private WebElement searchInput;

    @FindBy(css = "[value='Google Search']")
    private WebElement searchButton;
    public CustomWebElement getSearchButton() {
        return new CustomWebElement(searchButton);
    }

    @FindBy(css = "[value='I'm Feeling Lucky']")
    private WebElement luckyButton;
    public CustomWebElement getLuckyButton() {
        return new CustomWebElement(luckyButton);
    }

}
