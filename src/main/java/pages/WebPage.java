package pages;

import org.openqa.selenium.WebElement;
import step_definitions.Hooks;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class WebPage {

    protected static String CSS_SELECTOR;

    WebPage(String cssLocator) {
        CSS_SELECTOR = cssLocator;
        waitForPage();
        PageFactory.initElements(Hooks.webDriver, this);
    }

    protected static void waitForPage() {
        Hooks.webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(CSS_SELECTOR)));
    }

    public static void waitForVisibilityOf(WebElement webElement) {
        Hooks.webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
    }

}
