package step_definitions;

import cucumber.api.java8.En;
import pages.GoogleResultsPage;
import pages.GoogleSearchPage;

import static org.junit.Assert.assertTrue;

public class GoogleSearchStepDefs implements En {

    private GoogleSearchPage googleSearchPage;
    private GoogleResultsPage googleResultsPage;

    public GoogleSearchStepDefs() {

        When("^I navigate to the Google home page$", GoogleSearchPage::navigateTo);

        And("^I input (.*) into the search field$", (String inputValue) -> {
            googleSearchPage = new GoogleSearchPage();
            googleSearchPage.getSearchInput().sendKeys(inputValue);
        });

        And("^I click the Google Search button$", () -> {
            googleSearchPage.getSearchButton().click();
        });

        Then("^I verify the search results page is displayed$", () -> {
            googleResultsPage = new GoogleResultsPage();
            assertTrue(googleResultsPage.getNewsTab().isDisplayed());
            assertTrue(googleResultsPage.getVideosTab().isDisplayed());
            assertTrue(googleResultsPage.getImagesTab().isDisplayed());
        });

    }

}
