package step_definitions;

import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Output;

public class Hooks {

    public static WebDriver webDriver;
    public static WebDriverWait webDriverWait;
    private static final int MAX_WAIT_SECONDS = 10;
    private static boolean isBrowserOpen = false;

    @Before
    public void beforeTests() {
        if (isBrowserOpen) {
            webDriver.manage().deleteAllCookies();
            return;
        }
        Runtime.getRuntime().addShutdownHook(afterTests());
        Output.log("Opening new browser...");
        webDriver = new ChromeDriver(getChromeOptions());
        webDriverWait = new WebDriverWait(webDriver, MAX_WAIT_SECONDS);
        isBrowserOpen = true;
    }

    private Thread afterTests() {
        return new Thread(() -> {
            Output.log("Closing browser...");
            webDriver.manage().deleteAllCookies();
            webDriver.quit();
        });
    }

    private ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-fullscreen");
        return options;
    }

}
